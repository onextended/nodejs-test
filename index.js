// Dependencies
const fs = require('fs');
const http = require('http');
const https = require('https');
const express = require('express');
const booksRouter = require('./routes/books');

const path = require('path');
const $ = require('jquery');
const app = express();
const os = require('os');
const ip = require('ip');
let cors = require('cors');
let  url = require('url');
const logger = require('morgan');
let bodyParser = require('body-parser');
const navigator = require('navigator');
const MongoClient = require('mongodb').MongoClient;
const ObjectID = require('mongodb').ObjectID;
const assert = require('assert');
const dbName = 'userAccess';
const mongoose = require('mongoose');
const exphbs = require('express-handlebars');
const hbs = exphbs.create({
    defaultLayout: 'main',
    extname: 'hbs'
});
const Order = require('./models/Order');
const User = require('./models/User');
const cookieParser = require('cookie-parser');
const orderRouter = require('./routes/order');


// register engine
app.engine('hbs', hbs.engine);
app.set('view engine', 'hbs');
app.set('views', './views');
app.use(orderRouter);
app.use(express.urlencoded({
    extended: true
}));
app.use(express.static(__dirname + '/public'));
app.use(cookieParser);




async  function start() {
    try {
        await mongoose.connect('mongodb://localhost:27017/sitesData', {
            useNewUrlParser: true,
            useFindAndModify: false,
            useUnifiedTopology: true
        });
        console.log('MongoDB Started.');



app.use(logger());

// app.get('/', function (req, res) {
//     console.log(req.connection.remoteAddress.split('::ffff:')[1])
//     console.log('TRY!')
//     console.log(global.acc);
//
//
//
//     if (req.connection.remoteAddress.split('::ffff:')[1]+global.key == global.acc && global.requestAgent === global.ajaxAgent) {
//         // fs.readFile(path.join(__dirname, 'public', `index.html?&${global.query}`), (err, content) => {
//         //     if (err) {
//         //         res.writeHead(500);
//         //         res.end('Error');
//         //         // res.status(200).send('what???');
//         //     }
//         //     else {
//         //         res.writeHead(200, {
//         //             "Content-Type": 'text/html'
//         //         })
//         //         res.end(content);
//         //     }
//         //
//         // });
//
//
//
//             res.redirect(`/404.html`);
//
//     } else {
//
//         console.log('ERROR ACCESS')
//         // res.status(200).send('ERROR ACCESS');
//         fs.readFile(path.join(__dirname, 'public', `404.html`), (err, content) => {
//             if (err) {
//                 res.writeHead(500);
//                 res.end('Error');
//                 // res.status(200).send('what???');
//             }
//             else {
//                 res.writeHead(200, {
//                     "Content-Type": 'text/html'
//                 });
//                 res.end(content);
//             }
//
//         });
//     }
//
//
// });
// // app.get('/db', (req, res) => {
// //     db.collection('_access').find().toArray(function (err, docs) {
// //         if (err) {
// //             console.log(err);
// //             return   res.status(500);
// //         }
// //         res.send(docs);
// //     });
// // });
// // app.get('/:id', (req, res) => {
// //    db.collection('_access').findOne({_id: ObjectID}, (err, doc) => {
// //        if (err) {
// //            console.log(err);
// //            return res.status(500);
// //        }
// //        res.send(doc);
// //    });
// // });
// app.get(`/index.html`, function (req, res) {
//     console.log([req.params.id]);
//     console.log(`/make?&${global.final}`)
//     console.log(req.connection.remoteAddress.split('::ffff:')[1])
//     console.log('TRY!');
//     console.log(global.acc);
//     console.log(JSON.stringify(req.headers["user-agent"]), 'USERAGENT');
//     console.log(global.agent, 'MY - USERAGENT');
//
//     console.log(global.jsonFile, 'global.jsonFile');
//
//     let requestAgent = JSON.stringify(req.headers["user-agent"]);
//     let ajaxAgent = '"' + global.agent + '"';
//
//     global.requestAgent = requestAgent;
//     global.ajaxAgent = ajaxAgent;
//     console.log(requestAgent === ajaxAgent);
//
//     if (req.connection.remoteAddress.split('::ffff:')[1]+global.key == global.acc && requestAgent === ajaxAgent) {
//         fs.readFile(path.join(__dirname, 'public', `index.html`), (err, content) => {
//             if (err) {
//                 res.writeHead(500);
//                 res.end('Error');
//                 // res.status(200).send('what???');
//             }
//             else {
//                 res.writeHead(200, {
//                     "Content-Type": 'text/html'
//                 })
//                 res.end(content);
//             }
//
//         });
//
//     } else {
//
//         console.log('ERROR ACCESS')
//         // res.status(200).send('ERROR ACCESS');
//         fs.readFile(path.join(__dirname, 'public', `404.html`), (err, content) => {
//             if (err) {
//                 res.writeHead(500);
//                 res.end('Error');
//                 // res.status(200).send('what???');
//             }
//             else {
//                 res.writeHead(200, {
//                     "Content-Type": 'text/html'
//                 })
//                 res.end(content);
//             }
//
//         });
//     }
//
//
// });
//
// app.get('/getFile', function (req, res) {
//     console.log(req.connection.remoteAddress.split('::ffff:')[1])
//     console.log('TRY!');
//     console.log(global.acc);
//
//
//
//     if (req.connection.remoteAddress.split('::ffff:')[1]+global.key == global.acc && global.requestAgent === global.ajaxAgent) {
//         fs.readFile(path.join(__dirname, 'public', `data/${global.jsonFile}`), (err, content) => {
//             if (err) {
//                 // res.writeHead(500);
//                 // res.end('Error');
//                 res.status(200).send('what???');
//             }
//             else {
//                 res.writeHead(200, {
//                     "Content-Type": 'application/json'
//                 })
//                 res.end(content);
//             }
//
//         });
//     } else {
//         console.log('ERROR ACCESS')
//         res.status(200).send('ERROR ACCESS');
//     }
//
//
// });

// app.get('/get25', function (req, res) {
//     console.log(req.connection.remoteAddress.split('::ffff:')[1])
//     console.log('TRY!')
//     console.log(global.acc);
//
//     if (req.connection.remoteAddress.split('::ffff:')[1]+global.key == global.acc && global.requestAgent === global.ajaxAgent) {
//         fs.readFile(path.join(__dirname, 'public', `data/${global.acc}.json`), (err, content) => {
//             if (err) {
//                 // res.writeHead(500);
//                 // res.end('Error');
//                 res.status(200).send('what???');
//             }
//             else {
//                 res.writeHead(200, {
//                     "Content-Type": 'application/json'
//                 })
//                 res.end(content);
//             }
//
//         });
//     } else {
//         console.log('ERROR ACCESS')
//         res.status(200).send('ERROR ACCESS');
//     }
//
//
// });

// app.get(`/make`, async (req, res, next) => {
//
//
//
//
//
//     const orders = await Order.find({});
//
//
//
//     console.log(global.jsonFile, 'global.jsonFile');
//     console.log(global.file, 'global.file');
//     console.log(global.id, 'CREATED ID');
//     console.log(global.room, 'ROOOOOOM CREATED ID');
//
//
//
//     let requestAgent = JSON.stringify(req.headers["user-agent"]);
//     let ajaxAgent = '"' + global.agent + '"';
//
//     global.requestAgent = requestAgent;
//     global.ajaxAgent = ajaxAgent;
//     console.log(requestAgent === ajaxAgent);
//
//     // const orders = await Order.findOne({
//     //     ip: global.acc
//     // }, function (err, docs) {
//     //     // console.log(docs, 'ALL DB PARAMS');
//     //
//     //         res.render('index', {
//     //             title: "XTended | Make an order",
//     //             siteType: docs.siteType,
//     //         });
//     //
//     //     // res.cookie('item', docs.id);
//     //
//     //
//     // });
//
//     // const orders = await User.findOne({}, function (err, docs) {
//     //     console.log(docs, 'ALL DB PARAMS');
//     //
//     //     res.render('index', {
//     //         title: "XTended | Make an order",
//     //         siteType: docs.ip,
//     //     });
//     //
//     //     // res.cookie('item', docs.id);
//     //
//     //
//     // });
//
//
//
//     // if (req.connection.remoteAddress.split('::ffff:')[1]+global.key == global.acc && requestAgent === ajaxAgent) {
//     //
//     //
//     //
//     //
//     //
//     //
//     //     // db.collection('_access').find().toArray(function (err, docs) {
//     //     //     if (err) {
//     //     //         console.log(err);
//     //     //         return   res.status(500);
//     //     //     }
//     //     //     console.log(docs)
//     //     //     res.render('index', {
//     //     //         title: "XTended Web Studio",
//     //     //
//     //     //         script: 'get1.js',
//     //     //         file: 'get1.js',
//     //     //     });
//     //     // });
//     //
//     //
//     //
//     //
//     //
//     //     // fs.access(path.resolve(__dirname, 'public/data'), global.jsonFile, (err) => {
//     //     //     if (err) {
//     //     //         console.log('ERROR ACCESS');
//     //     //         // res.status(200).send('ERROR ACCESS');
//     //     //         fs.readFile(path.join(__dirname, 'public', `404.html`), (err, content) => {
//     //     //             if (err) {
//     //     //                 res.writeHead(500);
//     //     //                 res.end('Error');
//     //     //                 // res.status(200).send('what???');
//     //     //             } else {
//     //     //                 res.writeHead(200, {
//     //     //                     "Content-Type": 'text/html'
//     //     //                 })
//     //     //                 res.end(content);
//     //     //             }
//     //     //
//     //     //         });
//     //     //     } else {
//     //     //
//     //     //
//     //     //         if (fs.existsSync(path.resolve(__dirname, 'public/data', `receive${global.acc}.js`))) {
//     //     //             console.log('EXISTS!!!!')
//     //     //         } else {
//     //     //
//     //     //             fs.appendFile(path.resolve(__dirname, 'public/data', `receive${global.acc}.js`), `getAjax(\'https://order.xtended.studio/data/${global.jsonFile}\');`, (err) => {
//     //     //                 if (err) {
//     //     //                     throw err;
//     //     //                 } else {
//     //     //                     console.log('File was created');
//     //     //                 }
//     //     //
//     //     //             });
//     //     //             file = `data/receive${global.acc}.js`
//     //     //
//     //     //
//     //     //         }
//     //     //
//     //     //         res.render('index', {
//     //     //             title: "XTended Web Studio",
//     //     //             script: 'get.js',
//     //     //             file: file,
//     //     //         })
//     //     //
//     //     //     }
//     //     //
//     //     //
//     //     // });
//     //
//     // } else {
//     //     // res.render('index', {
//     //     //     title: "XTended Web Studio",
//     //     //     script: 'get.js',
//     //     //     file: file
//     //     // });
//     //     console.log('DID not work')
//     //
//     //     next();
//     // }
// });




// app.options('*', cors());
// app.use(cors());
// app.use((req,res,next)=>{
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//     if(req.method ==='OPTION'){
//         res.header("Access-Control-Allow-Methods", 'GET,POST,PUT,DELETE,PATCH');
//         return res.status(200).json({});
//     }
//     next();
// });
// app.use(function (req, res, next) {
//
//     req.header("Content-Security-Policy", "default-src 'self'");
//     res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE, options');
//     res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
//     res.setHeader('Content-Security-Policy', 'upgrade-insecure-requests');
//     res.setHeader('Access-Control-Allow-Credentials', true);
//     next();
// });
// app.use(function(req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//     next();
// });

// app.use(bodyParser.json());

let corsOptions = {
  origin: '*',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};
app.use(cors(corsOptions));


// app.post('/', async function(req, res){
//
// // const access = req.body.access;
//
//
//     const access = req.body.access;
//     const key = '-' + access.split('-')[1];
//     const userAgent = req.body.user;
//     const query = req.body.query;
//     const final = req.body.mainTypePrice;
//
//
//
//     global.acc = access;
//     global.key = key;
//     global.agent = userAgent;
//     global.query = query;
//     global.final = final;
//
//     // const user = new User({
//     //     ip: req.body.access,
//     //     userAgent: req.body.user,
//     // });
//
//     // await user.save().then(() => console.log('User was created'));
//     // await user.save((err, id) => {
//     //     console.log('Created ID IS:', id)
//     // });
//     // console.log(user);
//     console.log(req.body, 'MY POST PARAMS')
//     res.writeHead(200, {'content-type': 'application/json'});
//     const order = new Order({
//         ip: req.body.access,
//         siteType: req.body.typeSite,
//         siteTypePrice: req.body.typeSitePrice,
//     });
//
//     // await order.save((err, room) => {
//     //     try {
//     //         console.log('ROOM ID', room._id);
//     //         global.room = room._id;
//     //        res.cookie('item22', room._id);
//     //     }
//     // catch (e) {
//     //         console.log(e)
//     //     }
//     // });
//
//     await user.save().then(() => console.log('User was created'));
//     console.log('POST /');
//     console.dir(req.body);
//
//
//
//     // await order.save((err,room) =>){
//    //     t
//    //
//    //  });
//     // await order.save().then(() => console.log('Order params were saved'));
//
//     // const items = await Order.findOne({
//     //     ip: global.acc
//     // }, function (err, docs) {
//     //     console.log(docs, 'ALL DB PARAMS');
//     //
//     //     res.render('index', {
//     //         title: "XTended | Make an order",
//     //         siteType: docs.siteType,
//     //     });
//
//
//         // const items = await Order.findOne({
//     //     ip: global.acc
//     // }, function (err, docs) {
//     //     console.log(docs, 'ALL DB PARAMS');
//     //
//     //     res.render('index', {
//     //         title: "XTended | Make an order",
//     //         siteType: docs.siteType,
//     //     });
//
//
//
//
//
//
//     // res.redirect('/make');
//
//
//
//     //
//     // filePath = __dirname + `/public/data/${access}.json`;
//     // global.jsonFile = filePath.split('/data/')[1];
//     // fs.writeFile(filePath, JSON.stringify(req.body), function () {
//     //     res.end();
//     // });
//
//
//
//     // let getAccess = {
//     //     access: ,
//     // };
//
//     // db.collection('_access').insertOne(req.body)
//     //     .then(result => {
//     //         console.log(`Successfully inserted item with _id: ${result.insertedId}`);
//     //         global.file = result.ops[0];
//     //     })
//     //     .catch(err => console.error(`Failed to insert item: ${err}`));
//
//     // db.collection('_access').insert(getAccess, function(err, result) {
//     //     if (err) {
//     //         console.log(err);
//     //         res.status(500)
//     //     }
//     //     res.send(getAccess)
//     // });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//     // res.end()
// // });
//
//
//
//
//
// });


//         app.post('/', async function(req, res){
//
// // const access = req.body.access;
//
//
//             // const access = req.body.access;
//             // const key = '-' + access.split('-')[1];
//             // const userAgent = req.body.user;
//             // const query = req.body.query;
//             // const final = req.body.mainTypePrice;
//             //
//             //
//             //
//             // global.acc = access;
//             // global.key = key;
//             // global.agent = userAgent;
//             // global.query = query;
//             // global.final = final;
//
//             // const user = new User({
//             //     ip: req.body.access,
//             //     userAgent: req.body.user,
//             // });
//
//             // await user.save().then(() => console.log('User was created'));
//             // await user.save((err, id) => {
//             //     console.log('Created ID IS:', id)
//             // });
//             // console.log(user);
//             console.log(req.body, 'MY POST PARAMS')
//             const order = new Order({
//                 ip: req.body.access,
//                 siteType: req.body.typeSite,
//                 siteTypePrice: req.body.typeSitePrice,
//             });
//
//             // await order.save((err, room) => {
//             //     try {
//             //         console.log('ROOM ID', room._id);
//             //         global.room = room._id;
//             //        res.cookie('item22', room._id);
//             //     }
//             // catch (e) {
//             //         console.log(e)
//             //     }
//             // });
//
//             await order.save()
//             console.log('POST /');
//             console.dir(req.body);
//
//
//
//             // await order.save((err,room) =>){
//             //     t
//             //
//             //  });
//             // await order.save().then(() => console.log('Order params were saved'));
//
//             // const items = await Order.findOne({
//             //     ip: global.acc
//             // }, function (err, docs) {
//             //     console.log(docs, 'ALL DB PARAMS');
//             //
//             //     res.render('index', {
//             //         title: "XTended | Make an order",
//             //         siteType: docs.siteType,
//             //     });
//
//
//             // const items = await Order.findOne({
//             //     ip: global.acc
//             // }, function (err, docs) {
//             //     console.log(docs, 'ALL DB PARAMS');
//             //
//             //     res.render('index', {
//             //         title: "XTended | Make an order",
//             //         siteType: docs.siteType,
//             //     });
//
//
//
//
//
//
//             // res.redirect('/make');
//
//
//
//             //
//             // filePath = __dirname + `/public/data/${access}.json`;
//             // global.jsonFile = filePath.split('/data/')[1];
//             // fs.writeFile(filePath, JSON.stringify(req.body), function () {
//             //     res.end();
//             // });
//
//
//
//             // let getAccess = {
//             //     access: ,
//             // };
//
//             // db.collection('_access').insertOne(req.body)
//             //     .then(result => {
//             //         console.log(`Successfully inserted item with _id: ${result.insertedId}`);
//             //         global.file = result.ops[0];
//             //     })
//             //     .catch(err => console.error(`Failed to insert item: ${err}`));
//
//             // db.collection('_access').insert(getAccess, function(err, result) {
//             //     if (err) {
//             //         console.log(err);
//             //         res.status(500)
//             //     }
//             //     res.send(getAccess)
//             // });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//             // res.end()
// // });
//
//
//
//
//
//         });






// Starting both http & https servers
const httpServer = http.createServer(app);



    httpServer.listen(80, () => {
        console.log('HTTP Server running on port 80');
    });


    } catch (e) {
        console.log(e);
    }
}

start();

