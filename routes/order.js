const {Router} = require('express');
const router = Router();
const Order = require('../models/Order');
let bodyParser = require('body-parser');
let cors = require('cors');
// const cookieParser = require('cookie-parser');

router.use(bodyParser.urlencoded({
    extended: true
}));
router.use(bodyParser.json());
// router.use(cookieParser);



router.options('*', cors())
router.use(cors())
router.use((req,res,next)=>{
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    if(req.method ==='OPTION'){
        res.header("Access-Control-Allow-Methods", 'GET,POST,PUT,DELETE,PATCH');
        return res.status(200).json({});
    }
    next();
});
router.use(function (req, res, next) {

    req.header("Content-Security-Policy", "default-src 'self'");
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE, options');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Content-Security-Policy', 'upgrade-insecure-requests');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
router.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
var corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}
router.use(cors(corsOptions));



router.get('/make', async (req, res) => {
    // console.log(req, 'MY REQ')
    // console.log(res, 'MY RES')


    // const orders = await Order.find({});
    // res.cookie('itemID', global.idd);
    // console.log(req.cookies['cookie'], 'MY COKKIES');
    // console.log(req)
    // console.log(req.headers.cookie.split('itemID=')[1])

    const items = await Order.findOne({
        // _id: req.headers.cookie.split('itemID=')[1]
        _id: '5e456ff6f06a2e23f5aafb85',
    }, function (err, docs) {
        console.log(docs, 'ALL DB PARAMS');

        res.render('index', {
            title: "XTended | Make an order",
            siteType: docs.siteType,
        });


    });
            // res.render('index', {
            //     title: "XTended | Make an order",
            //     siteType: orders,
            //     siteTypePrice: orders
            // });
    // res.cookie('itemID', global.cookie);



});

router.post('/', async function(req, res, next){
   await console.log(req.body, 'MY POST PARAMS');
    const order = new Order({
        ip: req.body.access,
        siteType: req.body.typeSite,
        siteTypePrice: req.body.typeSitePrice,
    });
    console.log(order._id, 'ORDER ID');
    await res.cookie('rememberme', '1', {
        expires: new Date(Date.now() + 900000),
        httpOnly: true
    });
    // global.cookie = order.id;

    // res.send(order);
    // await res.cookie('itemID', order._id);





    // await Order.findOne({
    //     _id: order.id
    // }, function (err, docs) {
    //     console.log(docs, 'MY EXISTING ID');
    //
    // });


    // if ()


    await order.save().then(() => {
        res.send({order_id: order.id});
        console.log('User was created');
    } );



    next();

});


module.exports = router;