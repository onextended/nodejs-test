const { Schema, model } = require('mongoose');
const schema = new Schema({
    ip: {
        type: String,
        required: true,
        // unique: true,
    },
    userAgent: {
        type: String,
        required: true
    },


});

module.exports = model('Users', schema);