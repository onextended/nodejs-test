const { Schema, model } = require('mongoose');
const schema = new Schema({
    ip: {
        type: String,
        required: true
    },
    siteType: {
        type: String,
        required: true
    },
    siteTypePrice: {
        type: String,
        required: true
    },
    // mainType: {
    //     type: String,
    //     required: true
    // },
    // mainTypePrice: {
    //     type: String,
    //     required: true
    // },
    // titleDesign: {
    //     type: String,
    //     required: true
    // },
    // addTypeSite: {
    //     type: String,
    //     required: true
    // },
    // adaptParam: {
    //     type: String,
    //     required: true
    // },
    // adaptParamPrice: {
    //     type: String,
    //     required: true
    // },
    // user: {
    //     ref: 'Users',
    //     type: Schema.Types.ObjectID
    // }


});

module.exports = model('Order', schema);