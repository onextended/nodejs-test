
function getAjax(url) {
    let res = new XMLHttpRequest();
    res.open('GET', url);
    res.setRequestHeader("Content-Security-Policy", "upgrade-insecure-requests");
    res.setRequestHeader("x-content-security-policy", "upgrade-insecure-requests");
    res.setRequestHeader("x-webkit-csp", "upgrade-insecure-requests");
    res.send();
    res.onload = function () {
        if (res.status !== 200) {
            console.log(res.statusText);
        } else {
            console.log('Parsed')
            // console.log(JSON.parse(res.response));
            let data = JSON.parse(res.response);
            console.log(data)

            const typeSite = data.typeSite;
            const mainType = data.mainType;
            const addSiteType = data.addSiteType;
            const adaptSite = data.adaptSite;
            const typeSitePrice = data.typeSitePrice;
            const mainTypePrice = data.mainTypePrice;
            const adaptPrice = data.adaptPrice;
            const finalPrice = data.finalPrice;
            const finalDeadline = data.finalDeadline;
            const finalDeadlineDays = data.finalDeadlineDays;
            const getDesignType = data.getDesignType;
            const arrayOfItems = data.arrayOfItems;
            const designTitle = data.designTitle;
            const shortDeadline = data.shortDeadline;
            const shortValue = data.shortValue;
            const shortDays = data.shortDays;

            function renderItems() {
                const container = document.getElementById('info');
                const mainBlock = document.createElement('div');
                      mainBlock.className = 'order-list';
                const childBlock = document.createElement('div');
                      childBlock.className = 'order-info';

                      container.appendChild(mainBlock);

                const addContainer = document.createElement('div');
                      addContainer.className = 'order-more';
                container.appendChild(addContainer);

                const siteType = `  <div class="order-info">
                            <p class="p-header order-head no-mb">Вы выбрали тип сайта:</p>
                            <div class="site viewable type clicked clicked-order add">
                                <span class="site-title">${typeSite}</span>
                            </div>
                                <p class="s-str">${typeSitePrice} ₴</p>
                            </div>`;
                const tplSite = ` <div class="order-info">
                            <p class="p-header order-head no-mb">Основа сайта:</p>
                            <div class="site viewable type clicked clicked-order add ">
                                <span class="site-title">${mainType}</span>
                            </div>
                                <p class="s-str">${mainTypePrice} ₴</p>
                            </div>`;
                const tplType = `<div class="order-info">
                                <p class="p-header order-head no-mb">${designTitle}</p>
                                <div class="site viewable type clicked clicked-order addwt">
                                    <span class="site-title">${addSiteType}</span>
                                </div>
                            </div>`;
                const adaptParams = `    <div class="order-info">
                                <p class="p-header order-head no-mb">Адаптивность:</p>
                                <div class="site viewable type clicked clicked-order add">
                                    <span class="site-title">${adaptSite}</span>
                                </div>
                                <p class="s-str">${adaptPrice} ₴</p>
                            </div>`;
                console.log(arrayOfItems);

                const more = document.createElement('p');
                      more.textContent = arrayOfItems.length < 1 ? 'Дополнительные услуги не выбраны' : 'Дополнительно:';
                      more.className = 'moreItems';

                $(more).insertAfter($(mainBlock));


                let restClazz = data.shortValue == 'Не выбрано' ?  '' : 'viewed';
                const restInfo = `<div class="recentprice">
                            <p class="p-descrAdd no-mb" style="text-align: right">Примерная стоимость: ${finalPrice} ₴</p>
                            <p class="p-descrAdd no-mb" style="text-align: right">Примерные сроки: ${finalDeadline} ${finalDeadlineDays}</p>
                            <p class="p-descrAdd no-mb ${restClazz}" style="text-align: right; display: none;">Сокращённые сроки: ${shortValue} ${shortDays}</p>
                            </div>
`;



                mainBlock.innerHTML = siteType;
                mainBlock.innerHTML += tplSite;
                mainBlock.innerHTML += tplType;
                mainBlock.innerHTML += adaptParams;
                addContainer.innerHTML += arrayOfItems.join('');

                $(restInfo).insertAfter($(addContainer));


            }
            renderItems()





        }
    }
}


getAjax('https://order.xtended.studio/get25');




